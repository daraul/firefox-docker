From http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/

# Build the image, then do:
```bash
docker run -ti --rm \
       -e DISPLAY=$DISPLAY \
       -v /tmp/.X11-unix:/tmp/.X11-unix \
       firefox
```